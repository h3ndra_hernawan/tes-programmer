@extends('layouts.app')
@section('content')
    <div class="main-content-inner">
        <!-- sales report area start -->
        <div class="sales-report-area mt-5 mb-5">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#btn-add"><i class="ti-plus"></i>
                        &nbsp; Tambah User
                        </button>
                        @if(count($errors) > 0 )
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <ul class="p-0 m-0" style="list-style: none;">
                                    @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="data-tables">
                            <table id="dataTable" class="text-center" width="100%">
                                <thead class="bg-light text-capitalize">
                                    <tr>
                                        <th>No</th>
                                        <th>User</th>
                                        <th>Nama</th>
                                        <th>Status</th>
                                        <th>Level</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $counter = 0
                                    @endphp
                                    @foreach ($dateusers as $item)
                                    <tr>
                                        <td>{{ $counter += 1 }}</td>
                                        <td>{{ $item->user }}</td>
                                        <td>{{ $item->name }}</td>
                                        @if($item->status==1)
                                        <td>
                                            <div class="alert alert-success" role="alert">
                                                Aktif
                                            </div>
                                        </td>
                                        @else
                                        <td>
                                            <div class="alert alert-danger" role="alert">
                                                Tidak Aktif
                                            </div>
                                        </td>
                                        @endif
                                        @if ($item->level == 1)
                                            <td>Admin</td>
                                        @else
                                            <td>Pelamar</td>
                                        @endif
                                        <td>
                                            <form action="/user/{{ $item->id }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <div style="white-space: nowrap;">
                                                    <button type="button" class="btn btn-warning mb-3 edit" data-toggle="modal" data-target="#btn-edit"
                                                    id="{{ $item->id }}"><i class="ti-pencil"></i> Edit</button>
                                                    <button type="submit" class="btn btn-danger mb-3 hapus"  onclick="return confirm('Apakah yakin data akan Di Delete ?')"><i class="ti-trash"></i> Hapus</button>
                                                </div>
                                            </form>
                                        </td>
                                    
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- Modal Tambah-->
                    <div class="modal fade" id="btn-add">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <i class="ti-plus"></i>
                                    &nbsp; &nbsp; <h5 class="modal-title">Tambah User</h5>
                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                </div>
                                @if(count($errors) > 0 )
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <ul class="p-0 m-0" style="list-style: none;">
                                            @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="modal-body">
                                    <form action="{{ url('/user') }}" method="POST">
                                    @csrf
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-gp">
                                                        <label for="user">Email <span class="symbol required"></span></label>
                                                        <input type="email" required id="user" name="user" aria-required="true">
                                                        <i class="ti-user"></i>
                                                        <div class="text-danger"></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Password">Password<sup style="color: red">*</sup></label>
                                                        <div class="input-group mb-3">
                                                            <input type="password" id="password" class="form-control " name="password" placeholder="Masukan Password" required>
                                                            <div class="input-group-append">
                                                                <div class="input-group-text">
                                                                    <span id="show-password" class="fa fa-eye"></span>
                                                                </div>
                                                            </div> 
                                                        </div>
                                                        @if($errors->has('password'))
                                                            <span class="alert-message">Password Harus Diisi</span>
                                                            @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="nama">Nama<sup style="color: red">*</sup></label>
                                                        <input class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" type="text" placeholder="Masukan Nama" value="{{old('nama')}}"required>
                                                        @if($errors->has('nama'))
                                                            <span class="alert-message">{{$errors->first('nama')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="status">Status<sup style="color: red">*</sup></label>
                                                        <select class="form-control form-control-lg" name="status" required>
                                                        <option value="">Pilih</option>
                                                            <option value="1">Aktif</option>
                                                            <option value="2">Tidak Aktif</option>
                                                        </select>
                                                        @if($errors->has('status'))
                                                            <span class="alert-message">{{$errors->first('status')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="level">Level<sup style="color: red">*</sup></label>
                                                        <select class="form-control form-control-lg" name="level" required>
                                                        <option value="">Pilih</option>
                                                            <option value="1">Admin</option>
                                                            <option value="2">Pelamar</option>
                                                        </select>
                                                        @if($errors->has('level'))
                                                            <span class="alert-message">{{$errors->first('level')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </form>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal Edit-->
                    <div class="modal fade" id="btn-edit">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <i class="ti-plus"></i>
                                    &nbsp; &nbsp; <h5 class="modal-title">Edit User</h5>
                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                </div>
                                @if(count($errors) > 0 )
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <ul class="p-0 m-0" style="list-style: none;">
                                            @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="modal-body">
                                    <form id="form-edit" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <div class="card-body">
                                                <div class="row">
                                                <input type="hidden" id="userid_edit" class="form-control" name="userid">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label for="email">Email<sup style="color: red">*</sup></label>
                                                            <input class="form-control @error('user') is-invalid @enderror" id="edit_user" name="user" type="emails" placeholder="Masukan Username" value="{{old('user')}}"required>
                                                            @if($errors->has('user'))
                                                                <span class="alert-message">{{$errors->first('user')}}</span>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="Password">Password<sup style="color: red">*</sup></label>
                                                            <div class="input-group mb-3">
                                                                <input type="password" id="edit_password" class="form-control " name="password" placeholder="Masukan Password" required>
                                                                <div class="input-group-append">
                                                                    <div class="input-group-text">
                                                                        <span id="edit-password" class="fa fa-eye"></span>
                                                                    </div>
                                                                </div> 
                                                            </div>
                                                            @if($errors->has('password'))
                                                                <span class="alert-message">Password Harus Diisi</span>
                                                                @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="nama">Nama<sup style="color: red">*</sup></label>
                                                            <input class="form-control @error('nama') is-invalid @enderror" id="edit_nama" name="nama" type="text" placeholder="Masukan Nama" value="{{old('nama')}}"required>
                                                            @if($errors->has('nama'))
                                                                <span class="alert-message">{{$errors->first('nama')}}</span>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="status">Status<sup style="color: red">*</sup></label>
                                                            <select class="form-control form-control-lg" id='edit_status' name="status" required>
                                                            <option value="">Pilih</option>
                                                                <option value="1">Aktif</option>
                                                                <option value="2">Tidak Aktif</option>
                                                            </select>
                                                            @if($errors->has('status'))
                                                                <span class="alert-message">{{$errors->first('status')}}</span>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="level">Level<sup style="color: red">*</sup></label>
                                                            <select class="form-control form-control-lg" id ='edit_level'name="level" required>
                                                            <option value="">Pilih</option>
                                                                <option value="1">Admin</option>
                                                                <option value="2">Pelamar</option>
                                                            </select>
                                                            @if($errors->has('level'))
                                                                <span class="alert-message">{{$errors->first('level')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </form>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- overview area end -->
    </div>
@endsection
@section('javascript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script> 
@if( $massage = session('success'))
    <script type="text/javascript">
    Swal.fire({
        type: 'success',
        title:  "{{ $massage }}",
        text: 'success',
        timer: 4000,
    })
    </script>   
@endif
@if ( $massage = session('gagal'))
    <script type="text/javascript">
    Swal.fire({
        type: 'error',
        title:  "{{ $massage }}",
        text: 'Gagal',
        timer: 4000,
    })
@endif 
<script type="text/javascript">
    $('#show-password').click(function() {
      if ($(this).hasClass('fa-eye')) {
        $('#password').attr('type', 'text');
        $(this).removeClass('fa-eye');
        $(this).addClass('fa-eye-slash');
      } else {
        $('#password').attr('type', 'password');
        $(this).removeClass('fa-eye-slash');
        $(this).addClass('fa-eye');
      }
    })
    $('#edit-password').click(function() {
      if ($(this).hasClass('fa-eye')) {
        $('#edit_password').attr('type', 'text');
        $(this).removeClass('fa-eye');
        $(this).addClass('fa-eye-slash');
      } else {
        $('#edit_password').attr('type', 'password');
        $(this).removeClass('fa-eye-slash');
        $(this).addClass('fa-eye');
      }
    })
       $(document).on('click', '.edit', function(){  
            var id_user = $(this).attr('id')
            $('#form-edit').attr('action', '/user/' + id_user + '')
            document.getElementById('userid_edit').value = ''
            document.getElementById('edit_nama').value = ''
            document.getElementById('edit_user').value = ''
            document.getElementById('edit_status').value = ''
            document.getElementById('edit_level').value = ''
        

            $.ajax({
                url: '/user/' + id_user,
                success: function (res) {
                    console.log(res);
                        document.getElementById('userid_edit').value = res[0].id
                        document.getElementById('edit_user').value = res[0].user
                        document.getElementById('edit_nama').value = res[0].nama
                        document.getElementById('edit_status').value = res[0].status
                        document.getElementById('edit_level').value = res[0].level
                        $('#btn-edit').modal('show')
                }
        })

    });
</script>
@endsection