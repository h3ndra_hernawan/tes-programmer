@extends('layouts.app')
@section('content')
    <div class="main-content-inner">
        <!-- sales report area start -->
        <div class="sales-report-area mt-5 mb-5">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <!-- <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#btn-add"><i class="ti-plus"></i>
                        &nbsp; Tambah Data Pasien
                        </button> -->
                        @if(count($errors) > 0 )
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <ul class="p-0 m-0" style="list-style: none;">
                                    @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="data-tables">
                            <table id="dataTable" class="text-center" width="100%">
                                <thead class="bg-light text-capitalize">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Tempat</th>
                                        <th>Tanggal Lahir</th>
                                        <th>Posisi Yang Di Lamar</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $counter = 0
                                    @endphp
                                    @foreach ($datapelamar as $item)
                                    <tr>
                                        <td>{{ $counter += 1 }}</td>
                                        <td>{{ $item->nama }}</td>
                                        <td>{{ $item->tempat_lahir }}</td>
                                        <td>{{ date('d F Y', strtotime(($item->tgl_lahir)))}}</td>
                                        <td>{{ $item->posisi_lamar }}</td>
                                        <td>
                                            <form action="/DataPelamar/{{ $item->id }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <div style="white-space: nowrap;">
                                                    <a href="{{ url('/detaiPelamar/'.$item->id) }}" class="btn btn-primary mb-3"><i class="ti-user"> </i> Detail Pelamar</a>
                                                    <button type="submit" class="btn btn-danger mb-3 hapus"  onclick="return confirm('Apakah yakin data akan Di Delete ?')"><i class="ti-trash"></i> Hapus</button>
                                                </div>
                                            </form>
                                        </td>
                                    
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- Modal Tambah-->
                    <div class="modal fade" id="btn-add">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <i class="ti-plus"></i>
                                    &nbsp; &nbsp; <h5 class="modal-title">Tambah Data Pasien</h5>
                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                </div>
                                @if(count($errors) > 0 )
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <ul class="p-0 m-0" style="list-style: none;">
                                            @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="modal-body">
                                    <form action="{{ url('/DataPelamar') }}" method="POST">
                                        @csrf
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="nama_pasien">Nama Pasien<sup style="color: red">*</sup></label>
                                                        <input class="form-control @error('nama_pasien') is-invalid @enderror" id="nama_pasien" name="nama_pasien" type="text" value="{{old('nama_pasien')}}"required>
                                                        @if($errors->has('nama_pasien'))
                                                            <span class="alert-message">{{$errors->first('nama_pasien')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="alamat">Alamat<sup style="color: red">*</sup></label>
                                                        <textarea class="form-control @error('alamat') is-invalid @enderror" name="alamat" id="alamat" cols="6" rows="6" value="{{old('alamat')}}"required></textarea>
                                                        @if($errors->has('alamat'))
                                                            <span class="alert-message">{{$errors->first('alamat')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="no_tlp">No Tlp<sup style="color: red">*</sup></label>
                                                        <input class="form-control @error('no_tlp') is-invalid @enderror" id="no_tlp" type="number" name="no_tlp" value="{{old('no_tlp')}}"required>
                                                        <sup style="color: red">Min 11 karekter</sup>
                                                        <sup style="color: red">Max 12 karekter</sup>
                                                        @if($errors->has('no_tlp'))
                                                            <span class="alert-message">{{$errors->first('no_tlp')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="rt">Rt / Rw<sup style="color: red">*</sup></label>
                                                        <div class="form-row">
                                                            <div class="col-md-4 mb-3">
                                                                <input class="form-control @error('rt') is-invalid @enderror" id="rt" name="rt" type="number" value="{{old('rt')}}"required>
                                                                
                                                                <sup style="color: red">Max 3 karekter</sup>
                                                                @if($errors->has('rw'))
                                                                    <span class="alert-message">{{$errors->first('rw')}}</span>
                                                                @endif
                                                            </div>
                                                            /
                                                            <div class="col-md-4 mb-3">
                                                                <input class="form-control @error('rw') is-invalid @enderror" id="rw" name="rw" type="number" value="{{old('rw')}}"required>
                                                                
                                                                <sup style="color: red">Max 3 karekter</sup>
                                                                @if($errors->has('rw'))
                                                                    <span class="alert-message">{{$errors->first('rw')}}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="tgl_lahir">Tanggal Lahir<sup style="color: red">*</sup></label>
                                                        <input class="form-control @error('tgl_lahir') is-invalid @enderror" id="tgl_lahir" type="text" name="tgl_lahir" value="{{old('tgl_lahir')}}"required>
                                                        @if($errors->has('tgl_lahir'))
                                                            <span class="alert-message">{{$errors->first('tgl_lahir')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="jenis_kelamin">Jenis Kelamin<sup style="color: red">*</sup></label>
                                                        <select class="form-control form-control-lg" name="jenis_kelamin" required>
                                                        <option value="">Pilih</option>
                                                            <option value="Laki - Laki">Laki - Laki</option>
                                                            <option value="Perempuan">Perempuan</option>
                                                        </select>
                                                        @if($errors->has('jenis_kelamin'))
                                                            <span class="alert-message">{{$errors->first('jenis_kelamin')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </form>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal Edit-->
                    <div class="modal fade" id="btn-edit">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <i class="ti-plus"></i>
                                    &nbsp; &nbsp; <h5 class="modal-title">Edit kelurahan</h5>
                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                </div>
                                @if(count($errors) > 0 )
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <ul class="p-0 m-0" style="list-style: none;">
                                            @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="modal-body">
                                    <form id="form-edit" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <div class="card-body">
                                                <div class="row">
                                                    <input type="hidden" id="edit_id" class="form-control" name="id">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label for="nama_pasien">Nama Pasien<sup style="color: red">*</sup></label>
                                                            <input class="form-control @error('nama_pasien') is-invalid @enderror" id="edit_nama_pasien" name="nama_pasien" type="text" value="{{old('nama_pasien')}}"required>
                                                            @if($errors->has('nama_pasien'))
                                                                <span class="alert-message">{{$errors->first('nama_pasien')}}</span>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="alamat">Alamat<sup style="color: red">*</sup></label>
                                                            <textarea class="form-control @error('alamat') is-invalid @enderror" name="alamat" id="edit_alamat" cols="6" rows="6" value="{{old('alamat')}}"required></textarea>
                                                            @if($errors->has('alamat'))
                                                                <span class="alert-message">{{$errors->first('alamat')}}</span>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="no_tlp">No Tlp<sup style="color: red">*</sup></label>
                                                            <input class="form-control @error('no_tlp') is-invalid @enderror" id="edit_no_tlp" type="number" name="no_tlp" value="{{old('no_tlp')}}"required>
                                                            <sup style="color: red">Min 11 karekter</sup>
                                                            <sup style="color: red">Max 12 karekter</sup>
                                                            @if($errors->has('no_tlp'))
                                                                <span class="alert-message">{{$errors->first('no_tlp')}}</span>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="rt">Rt / Rw<sup style="color: red">*</sup></label>
                                                            <div class="form-row">
                                                                <div class="col-md-4 mb-3">
                                                                    <input class="form-control @error('rt') is-invalid @enderror" id="edit_rt" name="rt" type="number" value="{{old('rt')}}"required>
                                                                    
                                                                    <sup style="color: red">Max 3 karekter</sup>
                                                                    @if($errors->has('rw'))
                                                                        <span class="alert-message">{{$errors->first('rw')}}</span>
                                                                    @endif
                                                                </div>
                                                                /
                                                                <div class="col-md-4 mb-3">
                                                                    <input class="form-control @error('rw') is-invalid @enderror" id="edit_rw" name="rw" type="number" value="{{old('rw')}}"required>
                                                                    
                                                                    <sup style="color: red">Max 3 karekter</sup>
                                                                    @if($errors->has('rw'))
                                                                        <span class="alert-message">{{$errors->first('rw')}}</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="tgl_lahir">Tanggal Lahir<sup style="color: red">*</sup></label>
                                                            <input class="form-control @error('tgl_lahir') is-invalid @enderror" id="edit_tgl_lahir" type="text" name="tgl_lahir" value="{{old('tgl_lahir')}}"required>
                                                            @if($errors->has('tgl_lahir'))
                                                                <span class="alert-message">{{$errors->first('tgl_lahir')}}</span>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="jenis_kelamin">Jenis Kelamin<sup style="color: red">*</sup></label>
                                                            <select class="form-control form-control-lg" name="jenis_kelamin" id="edit_jenis_kelamin" required>
                                                                <option value="Laki - Laki">Laki - Laki</option>
                                                                <option value="Perempuan">Perempuan</option>
                                                            </select>
                                                            @if($errors->has('jenis_kelamin'))
                                                                <span class="alert-message">{{$errors->first('jenis_kelamin')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </form>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- overview area end -->
    </div>
@endsection
@section('javascript')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script> 
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

@if( $massage = session('success'))
    <script type="text/javascript">
    Swal.fire({
        type: 'success',
        title:  "{{ $massage }}",
        text: 'success',
        timer: 4000,
    })
    </script>   
@endif
@if ( $massage = session('gagal'))
    <script type="text/javascript">
    Swal.fire({
        type: 'error',
        title:  "{{ $massage }}",
        text: 'Gagal',
        timer: 4000,
    })
@endif 
<script type="text/javascript">
    $(document).ready(function () {
        
        $('#dataTable').DataTable();
        $("#tgl_lahir").datepicker({
            changeMonth : false,
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            onClose: function(dateText, inst) { 
                var day = $("#ui-datepicker-div .ui-datepicker-day :selected").val();
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('setDate', new Date(year, month,day, 6));
            }
        });
        $("#edit_tgl_lahir").datepicker({
            changeMonth : false,
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            rtl: true,
            onClose: function(dateText, inst) { 
                var day = $("#ui-datepicker-div .ui-datepicker-day :selected").val();
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('setDate', new Date(year, month,day, 6));
            }
        });
        $('#id_kelurahan').select2({
            theme: "classic",
            width: '100%',
            allowClear: true
        });
        $('#edit_id_kelurahan').select2({
            theme: "classic",
            width: '100%',
            allowClear: true
        });
        $('#jenis_kelamin').select2({
            theme: "classic",
            width: '100%',
            allowClear: true
        });
        $('#edit_jenis_kelamin').select2({
            theme: "classic",
            width: '100%',
            allowClear: true
        });
        });
       $(document).on('click', '.edit', function(){  
            var id= $(this).attr('id')
            $('#form-edit').attr('action', '/DataPelamar/' + id+ '')
            document.getElementById('edit_id').value = ''
            document.getElementById('edit_nama_pasien').value = ''
            document.getElementById('edit_alamat').value = ''
            document.getElementById('edit_no_tlp').value = ''
            document.getElementById('edit_rt').value = ''
            document.getElementById('edit_rw').value = ''
            document.getElementById('edit_id_kelurahan').value = ''
            document.getElementById('edit_tgl_lahir').value = ''
            document.getElementById('edit_jenis_kelamin').value = ''
        

            $.ajax({
                url: '/DataPelamar/' + id,
                success: function (res) {
                    console.log(res);
                        document.getElementById('edit_id').value = res[0].id
                        document.getElementById('edit_alamat').value = res[0].alamat
                        document.getElementById('edit_no_tlp').value = res[0].no_tlp
                        document.getElementById('edit_nama_pasien').value = res[0].nama_pasien
                        document.getElementById('edit_rt').value = res[0].rt
                        document.getElementById('edit_rw').value = res[0].rw
                        document.getElementById('edit_id_kelurahan').value = res[0].id_kelurahan
                        document.getElementById('edit_tgl_lahir').value = res[0].tgl_lahir
                        document.getElementById('edit_jenis_kelamin').value = res[0].jenis_kelamin
                        $('#btn-edit').modal('show')
                }
        })

    });

    var no_tlp = document.getElementById('no_tlp');
        no_tlp.addEventListener("keyup", function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            if(no_tlp.value.length >12){
                Swal.fire({
                    type: 'warning',
                    title:  "Maksimal 12 Karekter",
                    timer: 4000,
                })
                no_tlp.value = no_tlp.value.slice(0, 12);
            }
        });
        var alamat = document.getElementById('alamat');
        alamat.addEventListener("keyup", function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            if(alamat.value.length >255){
                Swal.fire({
                    type: 'warning',
                    title:  "Maksimal 255 Karekter",
                    timer: 4000,
                })
                alamat.value = alamat.value.slice(0, 255);
            }
        });
        var rt = document.getElementById('rt');
        rt.addEventListener("keyup", function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            if(rt.value.length >3){
                Swal.fire({
                    type: 'warning',
                    title:  "Maksimal 3 Karekter",
                    timer: 4000,
                })
                rt.value = rt.value.slice(0, 3);
            }
        });
        var rw = document.getElementById('rw');
        rw.addEventListener("keyup", function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            if(rw.value.length >3){
                Swal.fire({
                    type: 'warning',
                    title:  "Maksimal 3 Karekter",
                    timer: 4000,
                })
                rw.value = rw.value.slice(0, 3);
            }
        });

        var edit_no_tlp = document.getElementById('edit_no_tlp');
        edit_no_tlp.addEventListener("keyup", function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            if(edit_no_tlp.value.length >12){
                Swal.fire({
                    type: 'warning',
                    title:  "Maksimal 12 Karekter",
                    timer: 4000,
                })
                edit_no_tlp.value = edit_no_tlp.value.slice(0, 12);
            }
        });
        var edit_alamat = document.getElementById('edit_alamat');
        edit_alamat.addEventListener("keyup", function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            if(alamat.value.length >255){
                Swal.fire({
                    type: 'warning',
                    title:  "Maksimal 255 Karekter",
                    timer: 4000,
                })
                edit_alamat.value = edit_alamat.value.slice(0, 255);
            }
        });
        var edit_rt = document.getElementById('edit_rt');
        edit_rt.addEventListener("keyup", function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            if(edit_rt.value.length >3){
                Swal.fire({
                    type: 'warning',
                    title:  "Maksimal 3 Karekter",
                    timer: 4000,
                })
                edit_rt.value = edit_rt.value.slice(0, 3);
            }
        });
        var edit_rw = document.getElementById('edit_rw');
        edit_rw.addEventListener("keyup", function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            if(edit_rw.value.length >3){
                Swal.fire({
                    type: 'warning',
                    title:  "Maksimal 3 Karekter",
                    timer: 4000,
                })
                edit_rw.value = edit_rw.value.slice(0, 3);
            }
        });
</script>
@endsection