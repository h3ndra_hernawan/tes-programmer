<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title>Laporan Retribusi</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style>
    .page {
        width: 21cm;
        min-height: 29.7cm;
        padding-left: 2cm;
        padding-top: 2cm;
        padding-right: 2cm;
        padding-bottom: 2cm;
    }
    @page {
        size: A4 portrait;
        margin-left: 2cm;
        margin-top: 2cm;
        margin-right: 2cm;
        margin-bottom: 2cm;
    }
    table{
        border:1px solid #333;
        border-collapse:collapse;
        margin:0 auto;
    
    }
    td, tr, th{
        padding: 12px;
        font-size: 8pt;
        border:1px solid #333;

    }
    p{
        font-size: 9pt;
    }
    th{
        background-color: #f0f0f0;
    }
    h4, p{
        margin:0px;
    }
    @media screen {
       
       { display: none;}
    
    }
    @media print {
    
        table { page-break-after:auto }
        tr    { page-break-inside:avoid; page-break-after:auto }
        td    { page-break-inside:avoid; page-break-after:auto }
        thead { display:table-header-group }
        tfoot { display:table-footer-group }
                
    }

    @page {
      margin: 20px 30px 40px 50px;
    }
</style>
</head>
<body>            
    <table style="white-space: normal">
        <thead>
            @foreach ($datapasien as $item)
            <tr>
                <th>id Pasien:</th><td>{{ $item->id_pasien }}</td>
            </tr>
            <tr>
                <th>Nama Pasien:</th><td>{{ $item->nama_pasien }}</td>
            </tr>
            <tr>
                <th>Alamat:</th><td>{{ $item->alamat }}</td>
            </tr>
            <tr>
                <th>no Tlp:</th><td>{{ $item->no_tlp }}</td>
            </tr>
            <tr>
                <th>Rt / Rw:</th><td>{{ $item->rt }} / {{ $item->rw }}</td>
            </tr>
            <tr>
                <th>Nama Kelurahan:</th><td>{{ $item->nama_kelurahan }}</td>
            </tr>
            <tr>
                <th>Tgl Lahir:</th><td>{{ $item->tgl_lahir }}</td>
            </tr>
            <tr>
                <th>Jenis Kelamin:</th><td>{{ $item->jenis_kelamin }}</td>
            </tr>
            @endforeach
        </thead>
    </table>    
</body>
</html>