@extends('layouts.app')
@section('content')
    <div class="main-content-inner">
        <!-- sales report area start -->
        <div class="sales-report-area mt-5 mb-5">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h1><i class="ti-user"> </i>@php echo session()->get('nama') @endphp</h1>
                        <br>
                        <h1><i class="ti-email"> </i>@php echo session()->get('user') @endphp</h1>
                    </div> 
                </div>
            </div> 
        </div>
        <div class="sales-report-area mt-5 mb-5">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        @if(count($errors) > 0 )
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <ul class="p-0 m-0" style="list-style: none;">
                                    @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="data-tables">
                            <h2 style="text-align:center">Data Diri Pelamar</h2>
                            <div class="modal-body">
                                <form action="{{ url('/DataPelamar') }}" method="POST">
                                    @csrf
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <input type="hidden" name="id" value="<?php if (isset($datapelamar['id'])) echo $datapelamar['id']; ?>">
                                                    <label for="posisi_lamar">1.Posisi Yang Dilamar<sup style="color: red">*</sup></label>
                                                    <input class="form-control @error('posisi_lamar') is-invalid @enderror" id="posisi_lamar" name="posisi_lamar" type="text" value="<?php if (isset($datapelamar['posisi_lamar'])) echo $datapelamar['posisi_lamar']; ?>"required>
                                                
                                                    <span class="form-text text-danger" id="posisi_lamar" style="display: none">Posisi Yang Dilamar harus di pilih</span>
                                                    @if($errors->has('posisi_lamar'))
                                                        <span class="alert-message">{{$errors->first('posisi_lamar')}}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label for="nama">2.Nama<sup style="color: red">*</sup></label>
                                                    <input class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" type="text" value="<?php if (isset($datapelamar['nama'])) echo $datapelamar['nama']; ?>"required>
                                                    @if($errors->has('nama'))
                                                        <span class="alert-message">{{$errors->first('nama')}}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label for="no_ktp">3.No Ktp<sup style="color: red">*</sup></label>
                                                    <input class="form-control @error('no_ktp') is-invalid @enderror" id="no_ktp" name="no_ktp" type="number" value="<?php if (isset($datapelamar['no_ktp'])) echo $datapelamar['no_ktp']; ?>"required>
                                                    <sup style="color: red">Min 16 Digit</sup>
                                                    @if($errors->has('no_ktp'))
                                                        <span class="alert-message">{{$errors->first('no_ktp')}}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label for="tempat_lahir">4.Tempat / Tanggal Lahir<sup style="color: red">*</sup></label>
                                                    <div class="form-row">
                                                        <div class="col-md-4 mb-3">
                                                            <input class="form-control @error('tempat_lahir') is-invalid @enderror" id="tempat_lahir" name="tempat_lahir" type="text" value="<?php if (isset($datapelamar['tempat_lahir'])) echo $datapelamar['tempat_lahir']; ?>"required>
                                                            
                                                            @if($errors->has('tempat_lahir'))
                                                                <span class="alert-message">{{$errors->first('tempat_lahir')}}</span>
                                                            @endif
                                                        </div>
                                                        /
                                                        <div class="col-md-4 mb-3">
                                                            <input class="form-control @error('tgl_lahir') is-invalid @enderror" id="tgl_lahir" type="text" name="tgl_lahir" value="<?php if (isset($datapelamar['tgl_lahir'])) echo date('d-m-Y', strtotime($datapelamar['tgl_lahir'])); ?>"required>
                                                            @if($errors->has('tgl_lahir'))
                                                                <span class="alert-message">{{$errors->first('tgl_lahir')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="jenis_kelamin">5.Jenis Kelamin<sup style="color: red">*</sup></label>
                                                    <select class="form-control form-control-lg" id="jenis_kelamin" name="jenis_kelamin" required>
                                                    <option value="">Pilih</option>
                                                        <option value="Laki-Laki" <?php if (isset($datapelamar['jenis_kelamin']) &&$datapelamar['jenis_kelamin'] == 'Laki-Laki') echo 'selected'; ?>>Laki - Laki</option>
                                                        <option value="Perempuan" <?php if (isset($datapelamar['jenis_kelamin']) &&$datapelamar['jenis_kelamin'] == 'Perempuan') echo 'selected'; ?>>Perempuan</option>
                                                    </select>
                                                    @if($errors->has('jenis_kelamin'))
                                                        <span class="alert-message">{{$errors->first('jenis_kelamin')}}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label for="agama">6.Agama<sup style="color: red">*</sup></label>
                                                    <select class="form-control form-control-lg" id="agama" name="agama" required>
                                                    <option value="">Pilih</option>
                                                        <option value="islam" <?php if (isset($datapelamar['agama']) &&$datapelamar['agama'] == 'islam') echo 'selected'; ?>>islam</option>
                                                        <option value="Kristen" <?php if (isset($datapelamar['agama']) &&$datapelamar['agama'] == 'Kristen') echo 'selected'; ?>>Kristen</option>
                                                        <option value="Budha" <?php if (isset($datapelamar['agama']) &&$datapelamar['agama'] == 'Budha') echo 'selected'; ?>>Budha</option>
                                                        <option value="Hindu" <?php if (isset($datapelamar['agama']) &&$datapelamar['agama'] == 'Hindu') echo 'selected'; ?>>Hindu</option>
                                                    </select>
                                                    @if($errors->has('agama'))
                                                        <span class="alert-message">{{$errors->first('agama')}}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label for="golongan_darah">7.Golongan Darah<sup style="color: red">*</sup></label>
                                                    <select class="form-control form-control-lg" id="golongan_darah" name="golongan_darah" required>
                                                    <option value="">Pilih</option>
                                                        <option value="A" <?php if (isset($datapelamar['golongan_darah']) &&$datapelamar['golongan_darah'] == 'A') echo 'selected'; ?>>A</option>
                                                        <option value="A+" <?php if (isset($datapelamar['golongan_darah']) &&$datapelamar['golongan_darah'] == 'A+') echo 'selected'; ?>>A+</option>
                                                        <option value="AB" <?php if (isset($datapelamar['golongan_darah']) &&$datapelamar['golongan_darah'] == 'AB') echo 'selected'; ?>>AB</option>
                                                        <option value="B" <?php if (isset($datapelamar['golongan_darah']) &&$datapelamar['golongan_darah'] == 'B') echo 'selected'; ?>>B</option>
                                                        <option value="B+" <?php if (isset($datapelamar['golongan_darah']) &&$datapelamar['golongan_darah'] == 'B+') echo 'selected'; ?>>B+</option>
                                                        <option value="O" <?php if (isset($datapelamar['golongan_darah']) &&$datapelamar['golongan_darah'] == 'O') echo 'selected'; ?>>O</option>
                                                        <option value="O+" <?php if (isset($datapelamar['golongan_darah']) &&$datapelamar['golongan_darah'] == 'O+') echo 'selected'; ?>>O+</option>
                                                    </select>
                                                    @if($errors->has('golongan_darah'))
                                                        <span class="alert-message">{{$errors->first('golongan_darah')}}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label for="status">8.Status<sup style="color: red">*</sup></label>
                                                    <input class="form-control @error('status') is-invalid @enderror" id="status" name="status" type="text" value="<?php if (isset($datapelamar['status'])) echo $datapelamar['status']; ?>"required>
                                                    @if($errors->has('status'))
                                                        <span class="alert-message">{{$errors->first('status')}}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label for="alamat">9.Alamat KTP<sup style="color: red">*</sup></label>
                                                    <textarea class="form-control @error('alamat') is-invalid @enderror" name="alamat" id="alamat" cols="4" rows="6" value="{{old('alamat')}}"required><?php if (isset($datapelamar['alamat'])) echo $datapelamar['alamat']; ?></textarea>
                                                    @if($errors->has('alamat'))
                                                        <span class="alert-message">{{$errors->first('alamat')}}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label for="alamat_tinggal">10.Alamat Tinggal<sup style="color: red">*</sup></label>
                                                    <textarea class="form-control @error('alamat_tinggal') is-invalid @enderror" name="alamat_tinggal" id="alamat_tinggal" cols="4" rows="6" value="{{old('alamat_tinggal')}}"required><?php if (isset($datapelamar['alamat_tinggal'])) echo $datapelamar['alamat_tinggal']; ?></textarea>
                                                    @if($errors->has('alamat_tinggal'))
                                                        <span class="alert-message">{{$errors->first('alamat_tinggal')}}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label for="email">11.Email<sup style="color: red">*</sup></label>
                                                    <input class="form-control @error('email') is-invalid @enderror" id="email" name="email" type="email" value="<?php if (isset($datapelamar['email'])) echo $datapelamar['email']; ?>"required>
                                                    @if($errors->has('email'))
                                                        <span class="alert-message">{{$errors->first('email')}}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label for="no_tlp">12.No Tlp<sup style="color: red">*</sup></label>
                                                    <input class="form-control @error('no_tlp') is-invalid @enderror" id="no_tlp" type="number" name="no_tlp" value="<?php if (isset($datapelamar['no_tlp'])) echo $datapelamar['no_tlp']; ?>"required>
                                                    <!-- <sup style="color: red">Min 11 karekter</sup> -->
                                                    <sup style="color: red">Max 12 karekter</sup>
                                                    @if($errors->has('no_tlp'))
                                                        <span class="alert-message">{{$errors->first('no_tlp')}}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label for="orang_terdekat">13.Orang Terdekat Yang Dapat Dihubungi<sup style="color: red">*</sup></label>
                                                    <input class="form-control @error('orang_terdekat') is-invalid @enderror" id="orang_terdekat" name="orang_terdekat" type="text" value="<?php if (isset($datapelamar['orang_terdekat'])) echo $datapelamar['orang_terdekat']; ?>"required>
                                                    @if($errors->has('orang_terdekat'))
                                                        <span class="alert-message">{{$errors->first('orang_terdekat')}}</span>
                                                    @endif
                                                </div>
                                                @if(!empty($datapelamar['id']))
                                                <div class="form-group">
                                                    <label for="orang_terdekat">14.Pendidikan Terakhir<sup style="color: red">*</sup></label>
                                                    <div class="data-tables">
                                                        <table id="dataTablePendidikan" class="text-center" width="100%">
                                                            <thead class="bg-light text-capitalize">
                                                                <tr>
                                                                    <th>No</th>
                                                                    <th>Jengjang Pendidikan Terakhir</th>
                                                                    <th>Nama Institusi Akademik</th>
                                                                    <th>Jurusan</th>
                                                                    <th>Tahun Lulus</th>
                                                                    <th>Ipk</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @php
                                                                $counter = 0
                                                                @endphp
                                                                @foreach ($datapendidikan as $item)
                                                                <tr>
                                                                    <td>{{ $counter += 1 }}</td>
                                                                    <td>{{ $item->pendidikan_terakhir }}</td>
                                                                    <td>{{ $item->nama_institusi }}</td>
                                                                    <td>{{ $item->jurusan}}</td>
                                                                    <td>{{ $item->tahun_lulus}}</td>
                                                                    <td>{{ $item->ipk}}</td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="orang_terdekat">15.Riwayat Pelatihan<sup style="color: red">*</sup></label>
                                                    <div class="data-tables">
                                                        <table id="dataTablePelatihan" class="text-center" width="100%">
                                                            <thead class="bg-light text-capitalize">
                                                                <tr>
                                                                    <th>No</th>
                                                                    <th>Nama Kursus</th>
                                                                    <th>Sertifikat</th>
                                                                    <th>Tahun</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @php
                                                                $counter = 0
                                                                @endphp
                                                                @foreach ($datapelatihan as $item)
                                                                <tr>
                                                                    <td>{{ $counter += 1 }}</td>
                                                                    <td>{{ $item->nama_kursus}}</td>
                                                                    <td>{{ $item->sertifikat}}</td>
                                                                    <td>{{ $item->tahun}}</td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="orang_terdekat">16.Riwayat Pekerjaan<sup style="color: red">*</sup></label>
                                                    <div class="data-tables">
                                                        <table id="dataTablePekerjaan" class="text-center" width="100%">
                                                            <thead class="bg-light text-capitalize">
                                                                <tr>
                                                                    <th>No</th>
                                                                    <th>Nama Perusahaan</th>
                                                                    <th>Posisi Terakhir</th>
                                                                    <th>Pendapatan Terakhir</th>
                                                                    <th>Tahun</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @php
                                                                $counter = 0
                                                                @endphp
                                                                @foreach ($datapekerjaan as $item)
                                                                <tr>
                                                                    <td>{{ $counter += 1 }}</td>
                                                                    <td>{{ $item->nama_perusahaan }}</td>
                                                                    <td>{{ $item->posisi_terakhir }}</td>
                                                                    <td>{{ $item->pendapatan_terakhir}}</td>
                                                                    <td>{{ $item->tahun}}</td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                @endif
                                                <div class="form-group">
                                                    <label for="skil">17.Skil<sup style="color: red">*</sup></label>
                                                    <textarea class="form-control @error('skil') is-invalid @enderror" name="skil" id="skil" cols="4" rows="6" value="{{old('skil')}}"required><?php if (isset($datapelamar['skil'])) echo $datapelamar['skil']; ?></textarea>
                                                    @if($errors->has('skil'))
                                                        <span class="alert-message">{{$errors->first('skil')}}</span>
                                                    @endif
                                                    
                                                </div>
                                                <div class="form-group">
                                                    <label for="bersedia_ditempatkan">18.Bersedia Ditempatkan Diseluruh Kantor Perusahaan <sup style="color: red">*</sup></label>
                                                  
                                                        <input name="bersedia_ditempatkan" id="bersedia_ditempatkan_1" type="radio" required class="radio-col-indigo material-inputs" value="Ya"<?php if (isset($datapelamar['bersedia_ditempatkan']) && $datapelamar['bersedia_ditempatkan'] == 'Ya') {
                                                                                                                                                                        echo 'checked';
                                                                                                                                                                    } ?>>
                                                        <label for="bersedia_ditempatkan_1" class="mb-0 mt-2 custom-control-label">Ya</label>
                                                    
                                                  
                                                        <input name="bersedia_ditempatkan" id="bersedia_ditempatkan_2" type="radio" required class="radio-col-indigo material-inputs" value="Tidak"<?php if (isset($datapelamar['bersedia_ditempatkan']) && $datapelamar['bersedia_ditempatkan'] == 'Tidak') {
                                                                                                                                                                        echo 'checked';
                                                                                                                                                                    } ?>>
                                                        <label for="bersedia_ditempatkan_2" class="mb-0 mt-2 custom-control-label">Tidak</label>
                                               
                                                    @if($errors->has('bersedia_ditempatkan'))
                                                        <span class="alert-message">{{$errors->first('bersedia_ditempatkan')}}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label for="penghasilan">19.Penghasilan Yang Diharapkan<sup style="color: red">*</sup></label>
                                                    <input class="form-control price" id="penghasilan" name="penghasilan" onfocus='sum1()' onblur='sum()' value="<?php if (isset($datapelamar['penghasilan'])) echo $datapelamar['penghasilan']; else echo 0; ?>" required>
                                                    @if($errors->has('penghasilan'))
                                                        <span class="alert-message">{{$errors->first('penghasilan')}}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        @if(!empty($datapelamar['id']))
                                            <button type="submit" class="btn btn-primary">Update changes</button>
                                        @else
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        @endif
                                    </div>
                                </form>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- overview area end -->
    </div>
@endsection
@section('javascript')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script> 
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

@if( $massage = session('success'))
    <script type="text/javascript">
    Swal.fire({
        type: 'success',
        title:  "{{ $massage }}",
        text: 'success',
        timer: 4000,
    })
    </script>   
@endif
@if ( $massage = session('gagal'))
    <script type="text/javascript">
    Swal.fire({
        type: 'error',
        title:  "{{ $massage }}",
        text: 'Gagal',
        timer: 4000,
    })
@endif 
<script type="text/javascript">
    $(document).ready(function () {
        $('#dataTablePendidikan').DataTable();
        $('#dataTablePelatihan').DataTable();
        $('#dataTablePekerjaan').DataTable();
        $("#tgl_lahir").datepicker({
            changeMonth : false,
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            onClose: function(dateText, inst) { 
                var day = $("#ui-datepicker-div .ui-datepicker-day :selected").val();
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('setDate', new Date(year, month,day, 6));
            }
        });
        $('#golongan_darah').select2({
            theme: "classic",
            width: '100%',
            allowClear: true
        });
        $('#agama').select2({
            theme: "classic",
            width: '100%',
            allowClear: true
        });
        $('#jenis_kelamin').select2({
            theme: "classic",
            width: '100%',
            allowClear: true
        });
    var no_ktp = document.getElementById('no_ktp');
        no_ktp.addEventListener("keyup", function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            if(no_ktp.value.length >16){
                Swal.fire({
                    type: 'warning',
                    title:  "Maksimal 16 Karekter",
                    timer: 4000,
                })
                no_ktp.value = no_ktp.value.slice(0, 16);
            }
        });
    var no_tlp = document.getElementById('no_tlp');
        no_tlp.addEventListener("keyup", function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            if(no_tlp.value.length >12){
                Swal.fire({
                    type: 'warning',
                    title:  "Maksimal 12 Karekter",
                    timer: 4000,
                })
                no_tlp.value = no_tlp.value.slice(0, 12);
            }
        });
        var alamat = document.getElementById('alamat');
        alamat.addEventListener("keyup", function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            if(alamat.value.length >255){
                Swal.fire({
                    type: 'warning',
                    title:  "Maksimal 255 Karekter",
                    timer: 4000,
                })
                alamat.value = alamat.value.slice(0, 255);
            }
        });
        var alamat_tinggal = document.getElementById('alamat_tinggal');
        alamat_tinggal.addEventListener("keyup", function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            if(alamat_tinggal.value.length >255){
                Swal.fire({
                    type: 'warning',
                    title:  "Maksimal 255 Karekter",
                    timer: 4000,
                })
                alamat_tinggal.value = alamat_tinggal.value.slice(0, 255);
            }
        });
});
    
function sum() {
      var nom  = document.getElementById('penghasilan').value;
        if(nom==""){
            nom=0
        }
      document.getElementById('penghasilan').value = nom.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
      //nom.toLocaleString('en-US', {maximumFractionDigits:2});    
}
function sum1() {
      var nom  = document.getElementById('penghasilan').value;
      if(nom==""){
            nom=0
        }
      document.getElementById('penghasilan').value = nom.toString().replaceAll(",", "");
//nom.toLocaleString('en-US', {maximumFractionDigits:2});      
}       

</script>
@endsection