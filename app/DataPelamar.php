<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class DataPelamar extends Model
{
    use Notifiable;
    // public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $table = 'users';
//   protected $fillable = [
//         'data_pelamars'];  
    protected $fillable = [
            'id_user_login',
            'no_ktp', 
            'id_user_login',
            'posisi_lamar',
            'skil',
            'nama',
            'tempat_lahir',
            'tgl_lahir',
            'jenis_kelamin',
            'agama',
            'alamat_tinggal',
            'golongan_darah',
            'status',
            'alamat',
            'email',
            'no_tlp',
            'orang_terdekat',
            'bersedia_ditempatkan',
            'penghasilan'];

            
}
