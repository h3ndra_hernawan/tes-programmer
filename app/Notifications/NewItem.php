<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\SetoranHeader;
use App\User;

class NewItem extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $setoranheader,$user;
    public function __construct(SetoranHeader $setoranheader,User $user)
    {
        $this->setoranheader = $setoranheader;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    // public function toMail($notifiable)
    // {
    //     return (new MailMessage)
    //                 ->line('The introduction to the notification.')
    //                 ->action('Notification Action', url('/'))
    //                 ->line('Thank you for using our application!');
    // }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
                'userid' => $this->user->userid,
                'nama' => $this->user->nama,
                'status_bayar' => $this->setoranheader->status_bayar,
                'status_koreksi' => $this->setoranheader->status_koreksi,
                'req_koreksi' => $this->setoranheader->req_koreksi,
                'item' => $this->item->nama_user,
        ];
    }
}
