<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Validator;
use Hash;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('login.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     public function daftarUser(Request $request)
    {
         // dd($request->all());
         $request->validate([
            'user' => 'required|unique:users',
            'password' => 'required',
            'nama' => 'required',
            'status' => 'required',
            'level' => 'required'
        ]);
        $user  =  DB::table('users')->insert([
            'user' => $request->user,
            'password' => Hash::make($request->password),
            'name' => $request->nama,
            'status' => $request->status,
            'email' => $request->user,
            'level' => $request->level,
            'created_at' => new \DateTime()
        ]);
        if(!is_null($user)) {            
            return redirect('/')->with('success' , 'Data Sukses Tersimpan');
        }    
        else {
            return redirect('/')->with('gagal' , 'Data Gagal Tersimpan');
        }
    }
    public function postlogin(Request $request)
    {
        // dd($request->all());

            $this->validate($request,
            
            ['user'=>'required'],

            ['password'=>'required']  
        );
        $user      = $request->input('user');
        $password   = $request->input('password');

        $user      = $request->input('user');
        $password   = $request->input('password');
        if(Auth::guard('web')->attempt(['user' => $user, 'password' => $password, 'status' => 1])) {
            $users = DB::table('users')->where('user',$request->user)->get();
                foreach($users as $user){
                    $id = $user->id;
                    $user_id = $user->user;
                    $nama = $user->name;
                    $level = $user->level;
                    $status = $user->status;
                }
            Session::put('userid' , $id);
            Session::put('user' , $user_id);
            Session::put('level' , $level);
            Session::put('nama' , $nama);
            Session::put('status' , $status);
            // return "ok";
            return redirect('/dashboard')->with('success' , 'sukses login');
        } else if(Auth::guard('web')->attempt(['user' => $user, 'password' => $password, 'status' => 2])){
            return redirect('/')->with('gagal' , 'Username Sudah Tidak Aktive');
        }else {
            return redirect('/')->with('gagal' , 'Username atau Password Salah');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
