<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $dateusers = DB::table('users')->get(); 
        $page = 'user';
        return view('user.index',compact('page','dateusers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    

    public function store(Request $request)
    {
         // dd($request->all());
         $request->validate([
            'user' => 'required|unique:users',
            'password' => 'required',
            'nama' => 'required',
            'status' => 'required',
            'level' => 'required'
        ]);
        $user  =  DB::table('users')->insert([
            'user' => $request->user,
            'password' => Hash::make($request->password),
            'name' => $request->nama,
            'email' => $request->user,
            'status' => $request->status,
            'level' => $request->level,
            'created_at' => new \DateTime()
        ]);
        if(!is_null($user)) {            
            return redirect('user')->with('success' , 'Data Sukses Tersimpan');
        }    
        else {
            return redirect('user')->with('gagal' , 'Data Gagal Tersimpan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // return $id;
        $users = DB::table('users')
            ->where('id',$id)->get();   
        return $users;    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'user' => 'required',
            'password' => 'required',
            'nama' => 'required',
            'status' => 'required',
            'level' => 'required'
        ]);
        $users = DB::table('users')->where('id',$request->userid)->update([
            'user' => $request->user,
            'password' => Hash::make($request->password),
            'nama' => $request->nama,
            'status' => $request->status,
            'level' => $request->level,
            'updated_at' => new \DateTime()
        ]);
        if(!is_null($users)) {            
            return redirect('user')->with('success' , 'Data Sukses diperbaharui');
        }    
        else {
            return redirect('user')->with('gagal' , 'Data Gagal diperbaharui');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $level = DB::table('users')->where('id',$id)->delete();

        if(!is_null($level)) {            
            return redirect('user')->with('success' , 'Data Sukses Di Hapus');
        }    
        else {
            return redirect('user')->with('gagal' , 'Data Gagal Di Hapus');
        }
    }
}
