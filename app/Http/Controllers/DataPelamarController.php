<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use App\DataPelamar;
use App\DataMasterPosisi;
use PDF;


class DataPelamarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    
        if (session()->get('level') == 1){
            $datapelamar = DataPelamar::first();
        }else{
            $datapelamar = DataPelamar::where('id_user_login',session()->get('userid'))->first();
            
        }
        $datapendidikan = DB::table('data_pendidikans as a')
            ->join('data_pelamars as b', 'a.no_ktp', '=', 'b.no_ktp')
            ->select('a.pendidikan_terakhir','a.nama_institusi','a.jurusan','a.tahun_lulus','a.ipk')
            ->where('b.id_user_login',session()->get('userid') )
            ->get(); 
        $datapelatihan = DB::table('data_riwayat_pelatihans as a')
            ->join('data_pelamars as b', 'a.no_ktp', '=', 'b.no_ktp')
            ->select('a.nama_kursus','a.sertifikat','a.tahun')
            ->where('b.id_user_login',session()->get('userid') )
            ->get();
        $datapekerjaan = DB::table('data_riwayat_pekerjaans as a')
            ->join('data_pelamars as b', 'a.no_ktp', '=', 'b.no_ktp')
            ->select('a.nama_perusahaan','a.posisi_terakhir','a.pendapatan_terakhir','a.tahun')
            ->where('b.id_user_login',session()->get('userid') )
            ->get(); 
        $page = 'DataPelamar';
        return view('DataPelamar.index',compact('page','datapelamar','datapendidikan','datapelatihan','datapekerjaan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function DataPelamarAdmin()
    {
        $page = 'DataPelamar';
        $datapelamar = DataPelamar::select('id','nama','tempat_lahir','tgl_lahir','posisi_lamar')->get();
        return view('DataPelamarAdmin.index',compact('page','datapelamar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'posisi_lamar' => 'required',
            'no_ktp' =>  ['required', 'max:16'],
            'nama' => 'required',
        ]);
        // var_dump($request->all());
        $tgl_lahir =  date('Y-m-d', strtotime(($request->tgl_lahir)));
        $penghasilan =  (double)str_replace(',','',strval($request->penghasilan));
        $DataPelamar  =  DataPelamar::updateOrCreate([
            'id' => $request->id,
            'no_ktp' => $request->no_ktp,
        ], [
            'id_user_login' => session()->get('userid'),
            'posisi_lamar' => $request->posisi_lamar,
            'nama' => $request->nama,
            'no_ktp' => $request->no_ktp,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $tgl_lahir,
            'jenis_kelamin' => $request->jenis_kelamin,
            'agama' => $request->agama,
            'golongan_darah' => $request->golongan_darah,
            'status' => $request->status,
            'alamat' => $request->alamat,
            'alamat_tinggal' => $request->alamat_tinggal,
            'email' => $request->email,
            'no_tlp' => $request->no_tlp,
            'orang_terdekat' => $request->orang_terdekat,
            'skil' => $request->skil,
            'bersedia_ditempatkan' => $request->bersedia_ditempatkan,
            'penghasilan' => $penghasilan,
            'created_at' => new \DateTime(),
        ]);
        if (session()->get('level') == 1){
            if(!is_null($DataPelamar)) {            
                
                return redirect('DataPelamarAdmin')->with('success' , 'Data Sukses Tersimpan');
            }    
            else {
                return redirect('DataPelamarAdmin')->with('gagal' , 'Data Gagal Tersimpan');
            }

        }else{
            if(!is_null($DataPelamar)) {            
                
                return redirect('DataPelamar')->with('success' , 'Data Sukses Tersimpan');
            }    
            else {
                return redirect('DataPelamar')->with('gagal' , 'Data Gagal Tersimpan');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $DataPasien = DB::table('DataPasien')
            ->where('id_pasien',$id)->get();   
        return $DataPasien;    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function detaiPelamar($id)
    {
        $datapelamar = DataPelamar::where('id', '=',$id)->first();
        $datapendidikan = DB::table('data_pendidikans as a')
            ->join('data_pelamars as b', 'a.no_ktp', '=', 'b.no_ktp')
            ->select('a.pendidikan_terakhir','a.nama_institusi','a.jurusan','a.tahun_lulus','a.ipk')
            ->where('b.id_user_login',session()->get('userid') )
            ->get(); 
        $datapelatihan = DB::table('data_riwayat_pelatihans as a')
            ->join('data_pelamars as b', 'a.no_ktp', '=', 'b.no_ktp')
            ->select('a.nama_kursus','a.sertifikat','a.tahun')
            ->where('b.id_user_login',session()->get('userid') )
            ->get();
        $datapekerjaan = DB::table('data_riwayat_pekerjaans as a')
            ->join('data_pelamars as b', 'a.no_ktp', '=', 'b.no_ktp')
            ->select('a.nama_perusahaan','a.posisi_terakhir','a.pendapatan_terakhir','a.tahun')
            ->where('b.id_user_login',session()->get('userid') )
            ->get(); 
        $page = 'DataPelamar';
        return view('DataPelamarAdmin.detail',compact('page','datapelamar','datapendidikan','datapelatihan','datapekerjaan'));
    }
    
    public function print_pdf($id)
    {
        
        $datapasien = DB::table('DataPasien')
            ->join('kelurahan', 'DataPasien.id_kelurahan', '=', 'kelurahan.id')
            ->join('kota', 'kelurahan.id_kota', '=', 'kota.id')
            ->join('kecamatan', 'kelurahan.id_kecamatan', '=', 'kecamatan.id')
            ->select('DataPasien.id_pasien','DataPasien.nama_pasien','DataPasien.alamat','DataPasien.no_tlp', 'DataPasien.rt','DataPasien.rw','DataPasien.id_kelurahan','kelurahan.nama_kelurahan','DataPasien.tgl_lahir','DataPasien.jenis_kelamin','DataPasien.userInput','kota.nama_kota','kecamatan.nama_kecamatan')
            ->where('DataPasien.id_pasien', '=',$id)
            ->take(1)
            ->get(); 
        $pdf = PDF::loadView('DataPasien.print_pdf', compact('datapasien'))->setPaper('A4', 'landscape');
        return $pdf->stream('Data_pasien_'.$id.'.pdf');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $request->validate([
            'nama_pasien' => 'required',
            'alamat' => ['required', 'max:255'],
            'no_tlp' => ['required', 'min:11', 'max:12'],
            'rt' => ['required', 'max:3'],
            'rw' => ['required', 'max:3'],
            'id_kelurahan' => 'required',
            'tgl_lahir' => 'required',
            'jenis_kelamin' => 'required',
        ]);
        $tgl_lahir =  date('Y-m-d', strtotime(($request->tgl_lahir)));
        $DataPasien = DB::table('DataPasien')->where('id_pasien',$request->id_pasien)->update([
            'nama_pasien' => $request->nama_pasien,
            'alamat' => $request->alamat,
            'no_tlp' => $request->no_tlp,
            'rt' => $request->rt,
            'rw' => $request->rw,
            'id_kelurahan' => $request->id_kelurahan,
            'tgl_lahir' =>  $tgl_lahir ,
            'jenis_kelamin' => $request->jenis_kelamin,
            'userInput' => session()->get('nama') ,
            'updated_at' => new \DateTime()
        ]);
        if(!is_null($DataPasien)) {            
            return redirect('DataPasien')->with('success' , 'Data Sukses diperbaharui');
        }    
        else {
            return redirect('DataPasien')->with('gagal' , 'Data Gagal diperbaharui');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $level = DB::table('data_pelamars')->where('id',$id)->delete();

        if(!is_null($level)) {            
            return redirect('/DataPelamarAdmin')->with('success' , 'Data Sukses Di Hapus');
        }    
        else {
            return redirect('/DataPelamarAdmin')->with('gagal' , 'Data Gagal Di Hapus');
        }
    }
}
