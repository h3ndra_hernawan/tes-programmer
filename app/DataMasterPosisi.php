<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class DataMasterPosisi extends Model
{
    use Notifiable;
    // public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $table = 'users';
    protected $fillable = [
        'data_master_posisis'];
}
