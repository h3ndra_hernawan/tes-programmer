-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 19 Jan 2024 pada 00.42
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pelindo_tes`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_master_posisis`
--

CREATE TABLE `data_master_posisis` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_posisi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_pelamars`
--

CREATE TABLE `data_pelamars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_ktp` int(16) NOT NULL,
  `id_user_login` int(11) NOT NULL,
  `posisi_lamar` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat_lahir` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_lahir` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `jenis_kelamin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `golongan_darah` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat_tinggal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `skil` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_tlp` int(11) NOT NULL,
  `orang_terdekat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bersedia_ditempatkan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `penghasilan` decimal(19,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_pelamars`
--

INSERT INTO `data_pelamars` (`id`, `no_ktp`, `id_user_login`, `posisi_lamar`, `nama`, `tempat_lahir`, `tgl_lahir`, `jenis_kelamin`, `agama`, `golongan_darah`, `status`, `alamat`, `alamat_tinggal`, `skil`, `email`, `no_tlp`, `orang_terdekat`, `bersedia_ditempatkan`, `penghasilan`, `created_at`, `updated_at`) VALUES
(1, 111111, 1, 'tes edit 22', 'edit22', 'tes edit', '2024-01-18 17:00:00', 'Laki-Laki', 'islam', 'A', '111', 'ww edit', 'ww edit', '222222 edit', 'hendra66636@gmail.com', 111111, '2222', 'Ya', '10000000.00', '2024-01-18 10:25:55', '2024-01-18 16:27:31');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_pendidikans`
--

CREATE TABLE `data_pendidikans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_ktp` int(11) NOT NULL,
  `pendidikan_terakhir` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_institusi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jurusan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun_lulus` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ipk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_pendidikans`
--

INSERT INTO `data_pendidikans` (`id`, `no_ktp`, `pendidikan_terakhir`, `nama_institusi`, `jurusan`, `tahun_lulus`, `ipk`, `created_at`, `updated_at`) VALUES
(1, 111111, 'D4', 'Politeknik abc', 'mif', '2016', '3.16', NULL, NULL),
(2, 111111, 'D4', 'Politeknik cbd', 'mif', '2016', '3.16', NULL, NULL),
(3, 111111, 'D4', 'Politeknik abc', 'mif', '2016', '3.16', NULL, NULL),
(4, 111111, 'D4', 'Politeknik cbd', 'mif', '2016', '3.16', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_riwayat_pekerjaans`
--

CREATE TABLE `data_riwayat_pekerjaans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_ktp` int(11) NOT NULL,
  `nama_perusahaan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `posisi_terakhir` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pendapatan_terakhir` decimal(8,2) NOT NULL,
  `tahun` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_riwayat_pekerjaans`
--

INSERT INTO `data_riwayat_pekerjaans` (`id`, `no_ktp`, `nama_perusahaan`, `posisi_terakhir`, `pendapatan_terakhir`, `tahun`, `created_at`, `updated_at`) VALUES
(1, 111111, 'Pt abc', 'web dev', '999999.99', '2023', NULL, NULL),
(2, 111111, 'Pt cbd', 'web dev', '999999.99', '2023', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_riwayat_pelatihans`
--

CREATE TABLE `data_riwayat_pelatihans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_ktp` int(11) NOT NULL,
  `nama_kursus` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sertifikat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_riwayat_pelatihans`
--

INSERT INTO `data_riwayat_pelatihans` (`id`, `no_ktp`, `nama_kursus`, `sertifikat`, `tahun`, `created_at`, `updated_at`) VALUES
(1, 111111, 'LSP', 'bnsp', '2018', NULL, NULL),
(2, 111111, 'LSP', 'bnsp', '2019', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_skil_pelamars`
--

CREATE TABLE `data_skil_pelamars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_skil` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sekor_skil` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2024_01_18_124026_create_data_pelamar', 1),
(3, '2024_01_18_124049_create_data_pendidikan', 1),
(4, '2024_01_18_124107_create_data_riwayat_pelatihan', 1),
(5, '2024_01_18_124200_create_data_riwayat_pekerjaan', 1),
(6, '2024_01_18_124325_create_data_skil_pelamar', 1),
(7, '2024_01_18_124723_create_data_master_posisi', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `user`, `email`, `name`, `email_verified_at`, `password`, `status`, `level`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin@yahoo.com', 'admin@yahoo.com', 'admin', NULL, '$2y$10$Ei5.xLqz3gemvSmXGHcUn.3kjaum/1Wp4ksdHE8rxEKvX7KCHl2cy', '1', '1', NULL, '2024-01-18 09:24:47', NULL),
(2, 'user@yahoo.com', 'user@yahoo.com', 'user', NULL, '$2y$10$VRUijt5t.Sm2yi05RuO2we.TuehB8MmO1YdDolf.hRaYP7vmsEeNq', '1', '2', NULL, '2024-01-18 09:29:45', NULL),
(3, 'user1@yahoo.com', 'user1@yahoo.com', 'user', NULL, '$2y$10$wXyahAj2rhvyACCtncvu5em7Eyzapir2/w0a5.HZQIB/VZ8xMihga', '1', '2', NULL, '2024-01-18 10:48:46', NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `data_master_posisis`
--
ALTER TABLE `data_master_posisis`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `data_pelamars`
--
ALTER TABLE `data_pelamars`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `data_pendidikans`
--
ALTER TABLE `data_pendidikans`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `data_riwayat_pekerjaans`
--
ALTER TABLE `data_riwayat_pekerjaans`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `data_riwayat_pelatihans`
--
ALTER TABLE `data_riwayat_pelatihans`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `data_skil_pelamars`
--
ALTER TABLE `data_skil_pelamars`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_user_unique` (`user`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `data_master_posisis`
--
ALTER TABLE `data_master_posisis`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `data_pelamars`
--
ALTER TABLE `data_pelamars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `data_pendidikans`
--
ALTER TABLE `data_pendidikans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `data_riwayat_pekerjaans`
--
ALTER TABLE `data_riwayat_pekerjaans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `data_riwayat_pelatihans`
--
ALTER TABLE `data_riwayat_pelatihans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `data_skil_pelamars`
--
ALTER TABLE `data_skil_pelamars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
