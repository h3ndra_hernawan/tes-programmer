<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataPelamar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_pelamars', function (Blueprint $table) {
            $table->id();
            $table->integer('no_ktp');
            $table->integer('id_user_login');
            $table->string('posisi_lamar');
            $table->string('nama');
            $table->string('tempat_lahir');
            $table->timestamp('tgl_lahir');
            $table->string('jenis_kelamin');
            $table->string('agama');
            $table->string('golongan_darah');
            $table->string('status');
            $table->string('alamat');
            $table->string('alamat_tinggal');
            $table->string('skil');
            $table->string('email');
            $table->integer('no_tlp');
            $table->string('orang_terdekat');
            $table->string('bersedia_ditempatkan');
            $table->decimal('penghasilan', 19, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_pelamar');
    }
}
