<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataRiwayatPekerjaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_riwayat_pekerjaans', function (Blueprint $table) {
            $table->id();
            $table->integer('no_ktp');
            $table->string('nama_perusahaan');
            $table->string('posisi_terakhir');
            $table->decimal('pendapatan_terakhir', 8, 2);
            $table->string('tahun');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_riwayat_pekerjaan');
    }
}
