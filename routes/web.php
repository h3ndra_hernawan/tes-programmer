<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\NotificationController;


Route::group(['prefix' => '/' , 'middleware' => 'guest'], function () {
    Route::get('/', 'LoginController@index')->name('/');
    Route::post('/login.check_login', 'LoginController@postlogin')->name('login.check_login');  
    Route::post('/login.user_daftar', 'LoginController@daftarUser')->name('login.user_daftar'); 
  
}); 
Route::group(['prefix'=>'/','middleware' => 'check'], function () {
    Route::post('logout' , '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');
    Route::get('/dashboard', 'DashboardController@index')->name('/dashboard');
    Route::get('/user', 'UserController@index')->name('/user');
    Route::get('/DataPelamarAdmin', 'DataPelamarController@DataPelamarAdmin');

    Route::get('/print_pdf/{id}','DataPelamarController@print_pdf')->name('/print_pdf');
    Route::get('/detaiPelamar/{id}','DataPelamarController@detaiPelamar')->name('/detaiPelamar');


    Route::resource('user', 'UserController');
    Route::resource('kota', 'KotaController');
    Route::resource('kecamatan', 'KecamatanController');
    Route::resource('kelurahan', 'KelurahanController');
    Route::resource('DataPelamar', 'DataPelamarController');
}); 
